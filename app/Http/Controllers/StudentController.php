<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    public function index()
    {
        $students = DB::table('students')->get();

        return view('students.index', ['students' => $students]);
    }

    public function register()
    {
        // $courses =[ DB::table('courses')
        // ->select('students.*', 'courses.name')
        // ->leftJoin('students', 'students.id', '=', 'courses.id')
        // ->orderBy('courses.id')
        // ->get() ];

        // $courses =[ DB::table('courses')
        //     ->select('id', 'name')
        //     ->get()];

        $courses = [
            ['id' => 1, 'name' => 'Myanmar'],
            ['id' => 2, 'name' => 'English'],
            ['id' => 3, 'name' => 'Math'],
            ['id' => 4, 'name' => 'Physics'],
            ['id' => 5, 'name' => 'Chemistry'],
            ['id' => 6, 'name' => 'Bio'],
            ['id' => 7, 'name' => 'Eco'],
        ];

       // $course = DB::insert('insert into courses (id, name) values (?, ?)', [$courses['id'], $courses['name']]);
        return view('students.register', [
            'courses' => $courses])
        ;
    }

    public function create()
    {
        $student = new Student;
        $student->name = request()->name;
        $student->date_of_birth = request()->date_of_birth;
        $student->email = request()->email;
        $student->nrc = request()->nrc;
        $student->course_id = request()->course_id;
        $student = DB::insert('insert into students (name, date_of_birth, email, nrc, course_id, created_at, updated_at) values (?, ?,?, ?, ?, ?, ?)', [$student->name, $student->date_of_birth, $student->email, $student->nrc, $student->course_id, now(), now()]);
        return redirect('/students');
    }
}
