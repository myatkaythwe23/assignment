<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;

Route::get('/', [StudentController::class, 'index']);

Route::get('/students', [StudentController::class, 'index']);

Route::get('/students/register', [StudentController::class, 'register']);

Route::post('/students/register', [StudentController::class, 'create']);
