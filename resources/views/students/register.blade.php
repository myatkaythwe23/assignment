@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><b>Register</b></div>

                    <div class="card-body">
                        <form method="POST">
                            @csrf

                            <div class="row mb-3">
                                <label for="name" class="col-md-4">Enter your name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" required autofocus>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="date_of_birth" class="col-md-4">Enter your date of birth</label>

                                <div class="col-md-6">
                                    <input id="date_of_birth" type="date" class="form-control" name="date_of_birth"
                                        required>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="email" class="col-md-4">Enter your email</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" required>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="nrc" class="col-md-4">Your NRC no.</label>

                                <div class="col-md-6">
                                    <input id="nrc" type="text" class="form-control" name="nrc" required>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="course" class="col-md-4">Courses</label>

                                <div class="col-md-6">
                                    <select class="form-select" name="course_id" multiple>
                                        @foreach ($courses as $course)
                                            <option value="{{ $course['id'] }}">{{ $course['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">Register</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
