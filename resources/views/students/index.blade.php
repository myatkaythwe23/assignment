@extends('layouts.app')

@section('content')
    <h1>Student List</h1>
    <table>
        <tr>
            <th>Name</th>
            <th>Date of Birth</th>
            <th>Email</th>
            <th>NRC</th>
            <th>Courses</th>
        </tr>
        @foreach ($students as $student)
            <tr>
                <td>{{ $student->name }}</td>
                <td>{{ $student->date_of_birth }}</td>
                <td>{{ $student->email }}</td>
                <td>{{ $student->nrc }}</td>
                <td>{{ $student->course_id }}</td>
                {{-- <td class="list-group">
                    @foreach ($courses as $course)
                        <li class="list-group-item">
                            {{ $course->name }}
                        </li>
                    @endforeach --}}
                </td>
            </tr>
        @endforeach
    </table>
@endsection
